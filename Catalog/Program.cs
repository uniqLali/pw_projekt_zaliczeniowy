﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace CatalogNew
{
    class Program
    {
        static void Main(string[] args)
        {
            BLC.DataProvider dataProvider = new BLC.DataProvider(Catalog.Properties.Settings.Default.MyDatabase);

            Console.WriteLine("Chocolates");
            foreach (var item in dataProvider.GetChocolates)
            {
                Console.WriteLine(item.Name + " " + item.Calories);
            }
            Console.WriteLine();
            Console.WriteLine("Producers");
            foreach (var item in dataProvider.GetProducers)
            {
                Console.WriteLine(item.Name);
            }

            Console.Read();
        }
    }
}
