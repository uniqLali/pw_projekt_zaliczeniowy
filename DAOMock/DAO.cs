﻿using DAOMock.BO;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock
{
    public class DAO : IDAO
    {
        private List<IProducer> _producers;
        private List<IChocolate> _chocolates;

        public DAO()
        {
           _producers = new List<IProducer>()
            {
                new BO.ChocolateProducer() {Name="Nestlé" , Country="Switzerland",Url="https://www.nestle.pl/pl",  Email = "nestle@wp.com"},
                new BO.ChocolateProducer() {Name="Ferrero Group" , Country="Luxembourg",Url="https://www.ferrero.pl/", Email = "ferrero@ferrero.com"},
                new BO.ChocolateProducer() {Name="Teuscher" , Country="Switzerland",Url="http://www.teuscher.com/", Email = "teuscher@gmail.com"},
            };

            _chocolates = new List<IChocolate>()
            {
                new BO.Chocolate() {Name="White foundue",Producer = _producers[0],  Color= "White", Calories = 300, Type = Core.ChocolateType.White},
                new BO.Chocolate() {Name="Dark chocolate bar",Producer = _producers[1], Color= "Black",  Calories = 590,Type = Core.ChocolateType.Dark},
                new BO.Chocolate() {Name="Milk chocolate",Producer = _producers[2], Color= "Brown",Calories = 890, Type = Core.ChocolateType.Milk},
             };
        }

        public IEnumerable<IChocolate> GetAllChocolates()
        {
            return _chocolates;
        }

        public IEnumerable<IProducer> GetAllChocolateProducers()
        {
            return _producers;
        }

        public IChocolate NewChocolate()
        {
            return new Chocolate();
        }

        public IProducer NewChocolateProducer()
        {
            return new ChocolateProducer();
        }
    }
}
