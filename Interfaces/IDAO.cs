﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IDAO
    {
        IEnumerable<IChocolate> GetAllChocolates();
        IEnumerable<IProducer> GetAllChocolateProducers();
        IChocolate NewChocolate();
        IProducer NewChocolateProducer();
    }
}
