﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IChocolate
    {
        IProducer Producer { get; set; }
        string Name { get; set; }
        string Color { get; set; }
        int Calories { get; set; }
        ChocolateType Type { get; set; }
    }
}
