﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace BLC
{
    public class DataProvider
    {
        public IDAO _DAO { get; set; }

        public IEnumerable<IChocolate> GetChocolates
        {
            get { return _DAO.GetAllChocolates(); }
        }

        public IEnumerable<IProducer> GetProducers
        {
            get { return _DAO.GetAllChocolateProducers(); }
        }

        public IChocolate GetNewChocolate
        {
            get { return _DAO.NewChocolate(); }
        }

        public IProducer GetNewProducer
        {
            get { return _DAO.NewChocolateProducer();  }
        }

        public DataProvider(string databaseName)
        {
            var dllPath = Directory.GetCurrentDirectory() + @"\" + databaseName;
            Assembly assembly = null;

            try
            {
                assembly = Assembly.UnsafeLoadFrom(dllPath);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Error, database file not found!");
                System.Environment.Exit(1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                System.Environment.Exit(1);
            }

            foreach (var type in assembly.GetTypes())
            {
                if (_DAO != null)
                    break;

                foreach (var interfaceVar in type.GetInterfaces())
                {
                    if (interfaceVar != typeof(Interfaces.IDAO))
                        continue;

                    _DAO = (IDAO)Activator.CreateInstance(type, new object[] { });
                    break;

                }
            }
        }
    }
}

