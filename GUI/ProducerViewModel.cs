﻿using BLC;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Models
{
    public class ProducerViewModel : ViewModelBase
    {
        private readonly DataProvider _dataProvider;
        private IProducer _producer;

        public string ProducerName
        {
            get => _producer.Name;
            set
            {
                _producer.Name = value;
                OnPropertyChanged(nameof(ProducerName));
                Validate();
            }
        }

        public string ProducerEmail
        {
            get => _producer.Email;
            set
            {
                _producer.Email = value;
                OnPropertyChanged(nameof(ProducerEmail));
                Validate();
            }
        }

        public string ProducerCountry
        {
            get => _producer.Country;
            set
            {
                _producer.Country = value;
                OnPropertyChanged(nameof(ProducerCountry));
                Validate();
            }
        }

        public string ProducerUrl
        {
            get => _producer.Url;
            set
            {
                _producer.Url = value;
                OnPropertyChanged(nameof(ProducerUrl));
                Validate();
            }
        }

        public ProducerViewModel(IProducer producer)
        {
            _dataProvider = new DataProvider(Properties.Settings.Default.MyDatabase);
            _producer = producer;
            ProducerName = producer.Name;
            ProducerEmail = producer.Email;
            ProducerCountry = producer.Country;
            ProducerUrl = producer.Url;
        }

        public void Validate()
        {
            RemoveErrors(nameof(ProducerName));
            RemoveErrors(nameof(ProducerEmail));
            RemoveErrors(nameof(ProducerCountry));
            RemoveErrors(nameof(ProducerUrl));

            if (string.IsNullOrWhiteSpace(ProducerName))
            {
                AddError(nameof(ProducerName), "Podaj nazwę producenta.");
            }
            else
            {
                if (ProducerName.Length > 30)
                {
                    AddError(nameof(ProducerName), "Maksymalna długość nazwy producenta to 30 znaków.");
                }
            }

            if (string.IsNullOrWhiteSpace(ProducerEmail))
            {
                AddError(nameof(ProducerEmail), "Podaj adres email.");
            }
            else if(!ProducerEmail.Contains("@"))
            {
                AddError(nameof(ProducerEmail), "Mail musi zawierać znak @");
            }

            if (string.IsNullOrWhiteSpace(ProducerUrl))
            {
                AddError(nameof(ProducerUrl), "Podaj url");
            }
            else if(!ProducerUrl.Contains("."))
            {
                AddError(nameof(ProducerUrl), "Url musi zawierać kropkę!");
            }

            if (string.IsNullOrWhiteSpace(ProducerCountry))
            {
                AddError(nameof(ProducerCountry), "Podaj kraj producenta!");
            }

        }
    }
}
