﻿using BLC;
using Core;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Models
{
    public class ChocolateViewModel : ViewModelBase
    {
        private readonly DataProvider _dataProvider;
        private IChocolate _chocolate;

        public string ChocolateName
        {
            get => _chocolate.Name;
            set
            {
                _chocolate.Name = value;
                OnPropertyChanged(nameof(ChocolateName));
                Validate();
            }
        }

        public string ChocolateColor
        {
            get => _chocolate.Color;
            set
            {
                _chocolate.Color = value;
                OnPropertyChanged(nameof(ChocolateColor));
                Validate();
            }
        }

        public int ChocolateCalories
        {
            get => _chocolate.Calories;
            set
            {
                _chocolate.Calories = value;
                OnPropertyChanged(nameof(ChocolateCalories));
                Validate();
            }
        }

        public ChocolateType ChocolateType
        {
            get => _chocolate.Type;
            set
            {
                _chocolate.Type = value;
                OnPropertyChanged(nameof(ChocolateType));
                Validate();
            }
        }

        public ChocolateViewModel(IChocolate chocolate)
        {
            _dataProvider = new DataProvider(Properties.Settings.Default.MyDatabase);
            _chocolate = chocolate;
            ChocolateName = chocolate.Name;
            ChocolateColor = chocolate.Color;
            ChocolateCalories = chocolate.Calories;
            ChocolateType = chocolate.Type;
        }

        public void Validate()
        {
            RemoveErrors(nameof(ChocolateName));
            RemoveErrors(nameof(ChocolateColor));
            RemoveErrors(nameof(ChocolateCalories));
            RemoveErrors(nameof(ChocolateType));

            if (string.IsNullOrWhiteSpace(ChocolateName))
            {
                AddError(nameof(ChocolateName), "Podaj nazwę czekolady.");
            }
            else
            {
                if (ChocolateName.Length > 30)
                {
                    AddError(nameof(ChocolateName), "Maksymalna długość nazwy czekolady to 30 znaków.");
                }
            }

            if (string.IsNullOrWhiteSpace(ChocolateColor))
            {
                AddError(nameof(ChocolateColor), "Podaj kolor.");
            }

            
            if (ChocolateCalories < 0)
            {
                AddError(nameof(ChocolateCalories), "Liczba kalorii musi być dłuższa niż 0");
            }
        }
    }
}
