﻿using BLC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GUI.Models
{
    public class ProducerListViewModel : ViewModelBase
    {
        private readonly ListCollectionView _view;
        private readonly DataProvider _dataProvider;

        private ObservableCollection<ProducerViewModel> _producers;
        public ObservableCollection<ProducerViewModel> Producers
        {
            get => _producers;
            set
            {
                _producers = value;
                OnPropertyChanged(nameof(Producers));
            }
        }

        private ProducerViewModel _editedProducer;
        public ProducerViewModel EditedProducer
        {
            get => _editedProducer;
            set
            {
                _editedProducer = value;
                OnPropertyChanged(nameof(EditedProducer));
            }
        }

        public ProducerListViewModel()
        {
            _dataProvider = new DataProvider(Properties.Settings.Default.MyDatabase);
            _producers = new ObservableCollection<ProducerViewModel>();
            _view = (ListCollectionView)CollectionViewSource.GetDefaultView(_producers);

            foreach (var producer in _dataProvider.GetProducers)
            {
                _producers.Add(new ProducerViewModel(producer));
            }

            CreateProducerCommand = new RelayCommand(param => CreateProducer());
            SaveProducerCommand = new RelayCommand(param => SaveProducer());
            DeleteProducerCommand = new RelayCommand(param => DeleteProducer());
        }

        public RelayCommand CreateProducerCommand { get; }
        private void CreateProducer()
        {
            EditedProducer = new ProducerViewModel(_dataProvider.GetNewProducer)
            {
                ProducerName = "",
            };
        }

        public RelayCommand SaveProducerCommand { get; }
        private void SaveProducer()
        {
            if (EditedProducer != null)
            {
                if (!ProducerExist())
                {
                    _producers.Add(EditedProducer);
                }

                EditedProducer = null;
            }
        }

        public bool ProducerExist()
        {
            if (_producers.Any(c => c.ProducerName.ToLower().Equals(EditedProducer.ProducerName.ToLower())))
            {
                return true;
            }

            return false;
        }

        public RelayCommand DeleteProducerCommand { get; }
        private void DeleteProducer()
        {
            if (EditedProducer != null)
            {
                if (ProducerExist())
                {
                    _producers.Remove(EditedProducer);
                }

                EditedProducer = null;
            }
        }
    }
}
