﻿using BLC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GUI.Models
{
    public class ChocolateListViewModel : ViewModelBase
    {
        private readonly ListCollectionView _view;
        private readonly DataProvider _dataProvider;

        private ObservableCollection<ChocolateViewModel> _chocolates;
        public ObservableCollection<ChocolateViewModel> Chocolates
        {
            get => _chocolates;
            set
            {
                _chocolates = value;
                OnPropertyChanged(nameof(Chocolates));
            }
        }

        private ChocolateViewModel _editedChocolate;
        public ChocolateViewModel EditedChocolate
        {
            get => _editedChocolate;
            set
            {
                _editedChocolate = value;
                OnPropertyChanged(nameof(EditedChocolate));
            }
        }

        public ChocolateListViewModel()
        {
            _dataProvider = new DataProvider(Properties.Settings.Default.MyDatabase);
            _chocolates = new ObservableCollection<ChocolateViewModel>();
            _view = (ListCollectionView)CollectionViewSource.GetDefaultView(_chocolates);

            foreach (var chocolate in _dataProvider.GetChocolates)
            {
                _chocolates.Add(new ChocolateViewModel(chocolate));
            }

            CreateChocoalteCommand = new RelayCommand(param => CreateChocolate());
            SaveChocolateCommand = new RelayCommand(param => SaveChocolate());
            DeleteChocolateCommand = new RelayCommand(param => DeleteChocolate());
        }

        public RelayCommand CreateChocoalteCommand { get; }
        private void CreateChocolate()
        {
            EditedChocolate = new ChocolateViewModel(_dataProvider.GetNewChocolate)
            {
                ChocolateName = "",
            };
        }

        public RelayCommand SaveChocolateCommand { get; }
        private void SaveChocolate()
        {
            if (EditedChocolate != null)
            {
                if (!ChocolateExist())
                {
                    _chocolates.Add(EditedChocolate);
                }

                EditedChocolate = null;
            }
        }

        public bool ChocolateExist()
        {
            if (_chocolates.Any(c => c.ChocolateName.ToLower().Equals(EditedChocolate.ChocolateName.ToLower())))
            {
                return true;
            }

            return false;
        }

        public RelayCommand DeleteChocolateCommand { get; }
        private void DeleteChocolate()
        {
            if (EditedChocolate != null)
            {
                if (ChocolateExist())
                {
                    _chocolates.Remove(EditedChocolate);
                }

                EditedChocolate = null;
            }
        }
    }
}
