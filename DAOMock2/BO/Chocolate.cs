﻿using Core;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock2.BO
{
    public class Chocolate : IChocolate
    {
        public IProducer Producer { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int Calories { get; set; }
        public ChocolateType Type { get; set; }
    }
}
