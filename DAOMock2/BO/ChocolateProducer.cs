﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock2.BO
{
    public class ChocolateProducer : IProducer
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }

    }
}
