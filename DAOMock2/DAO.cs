﻿using DAOMock2.BO;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock2
{
    public class DAO : IDAO
    {

        private List<IProducer> _producers;
        private List<IChocolate> _chocolates;

        public DAO()
        {
            _producers = new List<IProducer>()
            {
                new BO.ChocolateProducer() {Name="Ah Cacao Real Chocolate" , Country="Mexico",Url="https://www.mexico.pl/pl",  Email = "mexico@wp.com"},
                new BO.ChocolateProducer() {Name="Amedei" , Country="Italy",Url="http://www.teuscher.com/", Email = "teuscher@gmail.com"},
            };

            _chocolates = new List<IChocolate>()
            {
                new BO.Chocolate() {Name="Dark chocolate",Producer = _producers[0], Color= "Black",  Calories = 590,Type = Core.ChocolateType.Dark},
                new BO.Chocolate() {Name="Green chocolate",Producer = _producers[1], Color= "Green",Calories = 890, Type = Core.ChocolateType.Milk},
             };
        }

        public IEnumerable<IChocolate> GetAllChocolates()
        {
            return _chocolates;
        }

        public IEnumerable<IProducer> GetAllChocolateProducers()
        {
            return _producers;
        }

        public IChocolate NewChocolate()
        {
            return new Chocolate();
        }

        public IProducer NewChocolateProducer()
        {
            return new ChocolateProducer();
        }
    }
}
